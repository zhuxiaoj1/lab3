// Lab 3 Danilo Zhu 1943382
package LinearAlgebra;

import org.junit.jupiter.api.Test;
import java.lang.Math;
import static org.junit.jupiter.api.Assertions.*;

class Vector3dTests {
    @Test
    void getterTests() {
        Vector3d testing = new Vector3d(2, 3, 5);

        assertEquals(2.0, testing.getX(), "X should equal 2.0");
        assertEquals(3.0, testing.getY(), "Y should equal 3.0");
        assertEquals(5.0, testing.getZ(), "Z should equal 5.0");
    }

    @Test
    void magnitudeTests() {
        Vector3d testing = new Vector3d(5, 9, 3);
        double actualMag = Math.sqrt(Math.pow(testing.getX(), 2) + Math.pow(testing.getY(), 2) + Math.pow(testing.getZ(), 2));

        assertEquals(actualMag, testing.magnitude(), "Magnitude should equal " + actualMag + ".");
    }

    @Test
    void dotProductTests() {
        Vector3d multiplier = new Vector3d(4, 1, 10);
        Vector3d multipicand = new Vector3d(0, 9, 6);

        Vector3d actualProd = new Vector3d(multiplier.getX() * multipicand.getX(), multiplier.getY() *
                multipicand.getY(), multiplier.getZ() * multipicand.getZ());
        Vector3d testProd = multiplier.dotProduct(multipicand);

        assertEquals(actualProd.getX(), testProd.getX(), "X should equal " + actualProd.getX());
        assertEquals(actualProd.getY(), testProd.getY(), "Y should equal " + actualProd.getY());
        assertEquals(actualProd.getZ(), testProd.getZ(), "Z should equal " + actualProd.getZ());
    }

    @Test
    void add() {
        Vector3d augend = new Vector3d(4, 10, 1);
        Vector3d addend = new Vector3d(2, 3, 5);

        Vector3d actualSum = new Vector3d(augend.getX() + addend.getX(), augend.getY() + addend.getY(), augend.getZ() + addend.getZ());
        Vector3d testSum = augend.add(addend);

        assertEquals(actualSum.getX(), testSum.getX(), "X should equal " + actualSum.getX());
        assertEquals(actualSum.getY(), testSum.getY(), "Y should equal " + actualSum.getY());
        assertEquals(actualSum.getZ(), testSum.getZ(), "Z should equal " + actualSum.getZ());
    }
}