// Lab 3 Danilo Zhu 1943382
package LinearAlgebra;

import java.lang.Math;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double magnitude() {
        double magnitude = Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
        return magnitude;
    }

    public Vector3d dotProduct(Vector3d multi) {
        Vector3d product = new Vector3d(x * multi.getX(), y * multi.getY(), z * multi.getZ());
        return product;
    }

    public Vector3d add(Vector3d addition) {
        Vector3d sum = new Vector3d(x + addition.getX(), y + addition.getY(), z + addition.getZ());
        return sum;
    }

    public static void main(String[] args) {
        Vector3d test1 = new Vector3d(2, 3, 5);
        Vector3d test2 = new Vector3d(5, 3, 2);

        System.out.println(test1.add(test2).toString());
    }

    public String toString() {
        return "x = " + getX() + " y = " + getY() + " z = " + getZ() + ".";
    }

}
